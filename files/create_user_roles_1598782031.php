<?php
class CREATEUSERROLES1598782031{
	public $table = 'roles';

	function up(){
		$str = "CREATE TABLE ".$this->table." (";
		$str .= 'id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, ';
		$str .= 'role varchar(255) NOT NULL';
		$str .= ")";
		return $str;

	}

	function down(){
		$str = 'DROP TABLE '.$this->table.';';
		return $str;
	}
}
?>