<?php
include "./SchemaCreator.php";
class ADDMOBILENOINUSERS1598784344 extends SchemaCreator{
	public $table = 'users';

	function up(){
		$this->addColumn('address varchar(255) NOT NULL');
		$this->updateColumn('password varchar(50) NOT NULL');
		$this->update();
	}

	function down(){
		$this->removeColumn('address');
		$this->updateColumn('password varchar(255) NOT NULL');
		$this->update();
	}
}
?>