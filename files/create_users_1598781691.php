<?php
include "./SchemaCreator.php";
class CREATEUSERS1598781691 extends SchemaCreator{
	public $table = 'users';

	function up(){
		$this->addColumn('id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY');
		$this->addColumn('email varchar(255) NOT NULL');
		$this->addColumn('password varchar(255) NOT NULL');
		$this->create();
	}

	function down(){
		$str = 'DROP TABLE '.$this->table.';';
		return $str;
	}

}
?>