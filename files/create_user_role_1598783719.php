<?php
include "./SchemaCreator.php";
class CREATEUSERROLE1598783719 extends SchemaCreator{
	public $table = 'user_roles';

	function up(){
		$this->addColumn('id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY');
		$this->addColumn('user_id INT(11) NOT NULL');
		$this->addColumn('role_id INT(11) NOT NULL');
		$this->create();
	}

	function down(){

	}
}
?>