<?php
	class SchemaCreator{

		public $addSchema = [];
		public $updateSchema = [];
		public $removeSchema = [];
		public $schemaStr = '';

		function addColumn($column){
			array_push($this->addSchema, $column);
		}

		function updateColumn($column){
			array_push($this->updateSchema, $column);
		}

		function removeColumn($column){
			array_push($this->removeSchema, $column);
		}

		function getColumns(){
			return implode(',', $this->addSchema);
		}

		function create(){
			$table = $this->table;
			$columns = $this->getColumns();
			$this->schemaStr = "CREATE TABLE $table ($columns)";
		}

		function update(){
			$table = $this->table;
			$columns = '';
			if(count($this->addSchema)){
				$columns .= 'ADD '.implode(', ADD', $this->addSchema);
			}
			if(count($this->updateSchema)){
				$columns .= ',MODIFY '.implode(', MODIFY', $this->updateSchema);
			}
			if(count($this->removeSchema)){
				$columns .= ',DROP COLUMN '.implode(', DROP COLUMN ', $this->removeSchema);
			}
			$columns = ltrim($columns,',');
			$this->schemaStr = "ALTER TABLE $table $columns";
		}

		function getSchema(){
			$this->up();
			return $this->schemaStr;
		}
		function getRollbackSchema(){
			$this->down();
			return $this->schemaStr;
		}
	}
?>