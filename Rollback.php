<?php

	trait Rollback
	{
		function __construct(){
		}

		public function rollback($rollback_limit=''){
			$rollback_limit = !empty($rollback_limit) ? $rollback_limit : 1;
			include("config/database.php");
			echo "Rollback...\n";
			$sql = "SELECT migration FROM migrations ORDER BY id DESC LIMIT $rollback_limit";
			$result = $conn->query($sql);
			if ($result->num_rows > 0) {
			  while($row = $result->fetch_assoc()) {
			  	include("files/$row[migration].php");

			  	$className = strtoupper($row['migration']);
				$className = preg_replace('/_/', '', $className);
			  	
			  	$obj = new $className();
			  	$sql = $obj->getRollbackSchema();
			  	if ($conn->query($sql) === TRUE) {
				  echo "migration $row[migration] rollback successfully\n";
				} else {
				  echo "Error: " . $sql . "<br>" . $conn->error;
				}

				$sql = 'UPDATE migrations SET status=0 where migration="'.$row['migration'].'"';
			  	if ($conn->query($sql) === TRUE) {
				  echo "migration $row[migration] updated successfully\n";
				} else {
				  echo "Error: " . $sql . "<br>" . $conn->error;
				}
			  }
			} else {
			  echo "0 results\n";
			}

			$conn->close();
		}

	}