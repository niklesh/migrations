<?php

	trait Migration
	{
		function __construct(){
		}

		public function migration(){
			include("config/database.php");
			echo "migration...\n";
			$sql = "SHOW CREATE TABLE `migrations`";
			$result = $conn->query($sql);
			if (!$result) {
				$sql = "CREATE TABLE `migrations` (
				  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
				  `migration` varchar(255) NOT NULL,
				  `status` integer(2) NOT NULL
				)";
				if ($conn->query($sql) === TRUE) {
				  echo "migrations created successfully\n";
				} else {
				  echo "Error: " . $sql . "<br>" . $conn->error;
				}
				
			} 
			$sql = "SELECT migration FROM migrations WHERE status=0";
			$result = $conn->query($sql);
			if ($result->num_rows > 0) {
			  while($row = $result->fetch_assoc()) {
			  	include("files/$row[migration].php");

			  	$className = strtoupper($row['migration']);
				$className = preg_replace('/_/', '', $className);
			  	
			  	$obj = new $className();
			  	$sql = $obj->getSchema();
			  	if ($conn->query($sql) === TRUE) {
				  echo "migration $row[migration] run successfully\n";
				} else {
				  echo "Error: " . $sql . "<br>" . $conn->error;
				}

				$sql = 'UPDATE migrations SET status=1 where migration="'.$row['migration'].'"';
			  	if ($conn->query($sql) === TRUE) {
				  echo "migration $row[migration] updated successfully\n";
				} else {
				  echo "Error: " . $sql . "<br>" . $conn->error;
				}
			  }
			} else {
			  echo "0 results\n";
			}

			$conn->close();
		}

	}