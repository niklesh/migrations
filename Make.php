<?php
	trait Make
	{
		function __construct(){
		}

		public function make($sub_command='',$file_name){
			$time = time();
			$migration_name = strtolower($file_name).'_'.$time;
			$className = strtoupper($file_name).$time;
			$className = preg_replace('/_/', '', $className);
			$content = "<?php";
			$content .= "\n";
			$content .= "include \"./SchemaCreator.php\";\n";
			$content .= "class $className extends SchemaCreator{\n";
			$content .= "\tpublic \$table = 'table_name';\n";
			$content .= "\n";
			$content .= "\tfunction up(){\n";
			$content .= "\n";
			$content .= "\t}\n";
			$content .= "\n";
			$content .= "\tfunction down(){\n";
			$content .= "\n";
			$content .= "\t}\n";
			$content .= "}\n";
			$content .= "?>";
			$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "files/".$migration_name.".php","wb");
			$response = fwrite($fp,$content);
			$this->addInQueue($migration_name);
			fclose($fp);
		}

		public function addInQueue($migration_name=''){
			include("config/database.php");
			$sql = "INSERT INTO migrations (migration, status)
			VALUES ('$migration_name', 0)";

			if ($conn->query($sql) === TRUE) {
			  echo "$migration_name created successfully";
			} else {
			  echo "Error: " . $sql . "<br>" . $conn->error;
			}
			$conn->close();
		}

	}